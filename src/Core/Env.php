<?php

namespace Aimedev\Elephaime\Core;

class Env {

	protected $data = [];
	public $dirs = [];

	public function __construct($filePath) {
		if (!file_exists($filePath))
			ErrorHandler::throw(500, 'The specified environment file does not exist');
		$this->parse($filePath);
	}

	/**
	 * Parse all environment variables from a specified file
	 * @param string $path
	 */
	private function parse(string $path) {
		$content = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) ?? [];
		foreach ($content as $line) {
			$equalPosition = strpos($line, '=');
			if ($equalPosition === false)
				continue;
			$key = substr($line, 0, $equalPosition);
			$value = substr($line, $equalPosition + 1);
			$this->data[$key] = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) !== null
				? filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
				: $value;
		}
	}

	/**
	 * Get or set an environment variable
	 * @param string $key
	 * @param null|mixed $value
	 */
	public function val(string $key, $value = null) {
		if ($value !== null) {
			$this->data[$key] = $value;
		}
		return $this->data[$key] ?? null;
	}
}
