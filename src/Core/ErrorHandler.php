<?php

namespace Aimedev\Elephaime\Core;

use Aimedev\Elephaime\Core\Render\PageRender;

class ErrorHandler {

	public static $errorContext = [];

	/**
	 * Enable the error handler
	 */
	public static function enable() {
		self::defaultReporting();

		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			if (php_sapi_name() === 'cli')
				return false;
			if (!(error_reporting() & $errno)) {
				return false;
			}

			$errstr = htmlspecialchars($errstr);
			switch ($errno) {
				case E_USER_ERROR:
					if (env('APP_DEBUG') == true) {
						throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
					} else {
						throw new \ErrorException('Server error', 500);
					}
					return true;
				case E_USER_WARNING:
					if (env('APP_DEBUG') == true) {
						throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
					} else {
						return false;
					}
					break;
				case E_USER_NOTICE:
					return false;
			}
			return true;
		});

		set_exception_handler(function($exception) {
			if (php_sapi_name() === 'cli')
				return false;

			$isDebug = env('APP_DEBUG') == true;

			// Logging error to a certain file.
			self::errorLog($exception);

			// Rendering error.
			$exceptionContext = !empty(self::$errorContext) ? self::$errorContext : [];
			$exceptionTrace = ($isDebug)
				? array_filter(explode('#', $exception->getTraceAsString()))
				: [];

			$errorFile = PageRender::getViewPath('errors/' . ($isDebug ? 'debug' : $exception->getCode()));
			if ($isDebug || file_exists($errorFile)) {
				$messageToDisplay = $exception->getMessage();
				if ($isDebug) {
					$messageToDisplay .= ' in ' . $exception->getFile();
					$messageToDisplay .= ', line ' . $exception->getLine();
				}
				self::render($exception->getCode(), $messageToDisplay, $exceptionTrace, $exceptionContext);
			} else {
				self::render(500, 'Server error', $exceptionTrace, $exceptionContext);
			}
			die();
		});
	}

	/**
	 * Disable the error handler
	 */
	public static function disable() {
		set_error_handler(null);
		set_exception_handler(null);
		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 0);
		error_reporting(0);
	}

	/**
	 * Enable default PHP error reporting
	 */
	public static function defaultReporting() {
		if (env('APP_DEBUG') == true) {
			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ALL);
		}
	}

	/**
	 * Add a log entry in the PHP errors file
	 */
	public static function errorLog($exception) {
		@mkdir(PROJECT_PATH . '/logs');
		$logFile = PROJECT_PATH . '/logs/php-errors.log';
		$errorString = "[" . date("d-M-Y H:i:s", $_SERVER['REQUEST_TIME']) . '] PHP ' .
			$exception->getCode() . ' - ' . $exception->getMessage() .
			" in " . $_SERVER['SCRIPT_FILENAME'] . " on line " . $exception->getLine() .
			$exception->getTraceAsString() . "\r\n";
		error_log($errorString, 3, $logFile);
	}

	/**
	 * Render an error as a HTML page
	 * @param int $errorCode
	 * @param string $message
	 * @param array $stacktrace
	 * @param array $context
	 */
	public static function render(int $errorCode, string $message, array $stacktrace, array $context = []) {
		ob_flush();
		ob_end_clean();

		$message = htmlspecialchars($message);
		$output = "$errorCode<br>$message";

		$isDebug = env('APP_DEBUG') == true;
		$filename = 'errors/' . ($isDebug ? 'debug' : $errorCode);

		if (!file_exists(PageRender::getViewPath($filename))) {
			$filename = $isDebug ? 'error-default-debug' : 'error-default';
		}

		http_response_code($errorCode);
		$extraVariables = [];

		$controllerPath = "App\\Controllers\\ErrorController";
		if (class_exists($controllerPath)) {
			$controller = new $controllerPath();
			$extraVariables['controller'] = $controller;
		}

		$extraVariables['responsive'] = (!empty($controller) && $controller->responsive === true);
		$extraVariables['title'] = (!empty($controller) && !empty($controller->title)
			? $controller->title
			: null);

		foreach ($stacktrace as &$entry) {
			$entry = '#' . $entry;
		}

		$extraVariables = array_merge([
			'code' => $errorCode,
			'message' => $message,
			'stacktrace' => $stacktrace,
			'context' => $context
		], $extraVariables);

		// Main file
		if ($filename === 'error-default-debug' || $filename === 'error-default') {
			extract($extraVariables);
			ob_start();
			include ELEPHAIME_INIT_PATH . $filename . '.php';
			$mainContent = ob_get_contents();
			ob_flush();
			ob_end_clean();
		} else {
			$mainContent = PageRender::getView(PageRender::getViewPath($filename), $extraVariables);
		}

		$htmlParts = [
			// Page content before </head> and just after <body>
			'head' => PageRender::getView('common.head', $extraVariables, false),
			'bodyBegin' => PageRender::getView('common.body-begin', $extraVariables, false),
			// Page content before </body> and just after </body>
			'bodyEnd' => PageRender::getView('common.body-end', $extraVariables, false),
			'foot' => PageRender::getView('common.foot', $extraVariables, false)
		];

		$output = PageRender::buildHTML($mainContent, $htmlParts, $extraVariables);

		if (lang()->isAvailable()) {
			$output = PageRender::fillLanguage($output);
		}
		return $output;
	}

	/**
	 * Use this function from your app to trigger errors to the end-user
	 * @param int $errorCode	HTTP error code
	 * @param string $message	Message to display
	 * @param array $context	Variables related to the error
	 */
	public static function throw(int $errorCode, string $message = 'Server error', array $context = []) {
		self::$errorContext = $context;
		throw new \ErrorException($message, $errorCode);
		die();
	}
}
