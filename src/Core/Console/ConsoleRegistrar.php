<?php

namespace Aimedev\Elephaime\Core\Console;

use Aimedev\Elephaime\Core\ErrorHandler;

class ConsoleRegistrar {

	private $tasks = [];
	private $commands = [];

	public function __construct() {}

	/**
	 * Check if a task is due
	 * @param string $timer
	 */
	private function isDue(string $timer) {
		$timer = explode(' ', $timer);
		switch ($timer[0]) {
			case 'every':
				if (in_array($timer[2], ['minute', 'minutes'])) {
					$currentMinute = date('i');
					if ($timer[1] === 'even') return $currentMinute % 2 === 0;
					if ($timer[1] === 'odd') return $currentMinute % 2 !== 0;
					if ($timer[1] === '*') return true;
					return is_int($currentMinute / min(intval($timer[1]), 60));
				}
				if (in_array($timer[2], ['hour', 'hours'])) {
					$currentHour = date('H');
					if ($timer[1] === 'even') return $currentHour % 2 === 0;
					if ($timer[1] === 'odd') return $currentHour % 2 !== 0;
					if ($timer[1] === '*') return true;
					return is_int($currentHour / min(intval($timer[1]), 24));
				}
				if (in_array($timer[2], ['day', 'days'])) {
					$currentDayOfMonth = date('d');
					if ($timer[1] === 'even') return $currentDayOfMonth % 2 === 0;
					if ($timer[1] === 'odd') return $currentDayOfMonth % 2 !== 0;
					if ($timer[1] === '*') return true;
					return is_int($currentDayOfMonth / min(intval($timer[1]), 31));
				}
				break;

			case 'at':
				if (strpos($timer[1], ':') > 0) { // e.g. 22:19
					$currentHour = date('H');
					$currentMinute = date('i');
					$queryHour = substr($timer[1], 0, strpos($timer[1], ':'));
					$queryMinute = substr($timer[1], strpos($timer[1], ':') + 1);
					$queryHourAll = in_array($queryHour, ['*', '**']);
					$queryMinuteAll = in_array($queryMinute, ['*', '**']);
					if ($queryHourAll && $queryMinuteAll) return true;
					if ($queryHourAll) return $queryMinute === $currentMinute;
					if ($queryMinuteAll) return $queryHour === $currentHour;
					return ($currentHour . ':' . $currentMinute) === $timer[1];
				}
				break;
		}
		return false;
	}

	/**
	 * Check all tasks to perform those that are due
	 */
	public function checkTasks() {
		$toDoTasks = [];
		foreach ($this->tasks as $task) {
			if (!$this->isDue($task['timer']))
				continue;
			$toDoTasks[] = $task;
		}
		foreach ($toDoTasks as $task) {
			$task->execute();
		}
	}

	/**
	 * Check all commands to answer the one sent from CLI
	 */
	public function answerCommand() {
		global $argv;
		$currentCommand = !empty($argv[1])
			? (string) $argv[1]
			: '';

		foreach ($this->commands as $command) {
			if ($command->name === $currentCommand) {
				$command->execute();
				return;
			}
		}

		ErrorHandler::throw(500, 'No console command found for your request');
	}

	/**
	 * Set a new scheduled task
	 * @param string $timer
	 * @param \Closure $closure
	 */
	public function task(string $timer, \Closure $closure) {
		if (empty($timer) || empty($closure))
			return;
		$task = new Task($timer, $closure);
		$this->tasks[] = $task;
		return $task;
	}

	/**
	 * Set a new CLI command
	 * @param string $command
	 * @param mixed $controller
	 */
	public function command(string $command = '', $controller) {
		if (empty($command) || empty($controller))
			return;
		$command = new Command($command, $controller);
		$this->commands[] = $command;
		return $command;
	}
}
