<?php

namespace Aimedev\Elephaime\Core\Routing;

class Route {

	public $identifier = -1;
	public $customName = '';
	public $method = '';
	public $path = '';

	public $closure = null;
	public $parameters = [];

	public $befores = [];
	public $afters = [];

	public function __construct(int $identifier, $method, $path, $closure) {
		$this->identifier = $identifier;
		$this->method = $method;
		$this->path = $path;
		$this->closure = $closure;
	}

	/**
	 * Redirect the user to this route
	 */
	public function go() {
		header('Location: ' . $this->path);
		die();
	}

	/**
	 * Add a before closure executed before the route main closure
	 * @param string|Closure $stringOrClosure
	 */
	public function before($stringOrClosure) {
		$closureData = RouteRegistrar::formatClosureForRoute($stringOrClosure);
		$this->befores[] = RouteRegistrar::getMethodFromClosureData($closureData);
		return $this;
	}

	/**
	 * Add an after closure executed after the route main closure
	 * @param string|Closure $stringOrClosure
	 */
	public function after($stringOrClosure) {
		$closureData = RouteRegistrar::formatClosureForRoute($stringOrClosure);
		$this->afters[] = RouteRegistrar::getMethodFromClosureData($closureData);
		return $this;
	}
}
