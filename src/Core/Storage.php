<?php

namespace Aimedev\Elephaime\Core;

class Storage {

	private static $projectDirectory = PROJECT_PATH;
	private static $appDirectory = PROJECT_PATH . 'app/';
	private static $appAssetsDirectory = PROJECT_PATH . 'app/Assets/';
	private static $storageDirectory = PROJECT_PATH . 'storage/';
	private static $wwwDirectory = PROJECT_PATH . 'www/';
	private static $cacheDirectory = 'cache/';
	private static $diskDirectory = 'disk/';
	private static $staticDirectory = 'static/';

	/**
	 * Remove a directory content with a recursive strategy
	 * @param string $path
	 * @param bool $keepRootDir
	 */
	private static function rmdirRecursive(string $path, bool $keepRootDir = false) {
		$files = glob($path . '/*');
		foreach ($files as $file) {
			(is_dir($file) && !is_link($file))
				? self::rmdirRecursive($file)
				: unlink($file);
		}
		if (!$keepRootDir)
			rmdir($path);
	}

	/**
	 * Copy a directory content with a recursive strategy
	 * @param string $fromPath
	 * @param string $toPath
	 * @param bool $isFromBrain		For the assets CLI command
	 */
	private static function copyDirRecursive(string $fromPath, string $toPath, bool $isFromBrain = false) {
		if (!is_dir($fromPath))
			return;
		$files = array_diff(scandir($fromPath), array('..', '.'));
		foreach ($files as $file) {
			if (is_link($file))
				continue;
			if (is_dir($fromPath . '/' . $file)) {
				mkdir($toPath . '/' . $file);
				self::copyDirRecursive($fromPath . '/' . $file . '/', $toPath . '/' . $file . '/');
			} else {
				copy($fromPath . '/' . $file, $toPath . '/' . $file);
				if ($isFromBrain && strpos($file, '.scss') === strlen($file) - 5) { // SCSS to CSS
					rename($toPath . '/' . $file, $toPath . '/' . preg_replace('/\.scss$/', '.css', $file));
				}
			}
		}
	}

	/**
	 * Chroot a file path
	 * @param string $path
	 */
	private static function sanitizeFilePath(string $path) {
		$path = str_replace('..', '', $path); // Chroot file in the path
		$path = preg_replace('/^[\^*?"<>|:]*$/', '', $path);
		return $path;
	}

	/**
	 * Delete a cache directory content
	 * @param string $path
	 */
	private static function removeCacheDirectory(string $path) {
		self::rmdirRecursive($path);
	}

	/**
	 * Automatic cleaning for the cache directory
	 */
	public static function clearCache() {
		if (!is_dir(self::getCacheDirectory())) {
			ErrorHandler::throw(500, 'No cache directory for this project - please run the initializer script');
		}

		// All cache directories
		$entries = array_diff(scandir(self::getCacheDirectory()), array('.', '..'));
		foreach ($entries as $entry) {
			$lastModified = filemtime(self::getCacheDirectory() . '/' . $entry);
			if (is_dir(self::getCacheDirectory() . $entry) && time() - $lastModified > 300) {
				self::removeCacheDirectory(self::getCacheDirectory() . '/' . $entry);
			}
		}

		// User cache directory
		if (!is_dir(self::getUserCacheDirectory()))
			return;
		$entries = array_diff(scandir(self::getUserCacheDirectory()), array('.', '..'));
		foreach ($entries as $entry) {
			$lastModified = filemtime(self::getUserCacheDirectory() . '/' . $entry);
			if (is_dir(self::getUserCacheDirectory() . $entry) && time() - $lastModified > 300) {
				self::removeCacheDirectory(self::getUserCacheDirectory() . '/' . $entry);
			}
		}
	}

	/**
	 * Initialize the cache directory for the current user
	 */
	public static function initializeCacheForSession() {
		$fileDir = self::getUserCacheDirectory();
		if (!is_dir($fileDir))
			mkdir($fileDir);
	}

	public static function getProjectDirectory() {
		return self::$projectDirectory;
	}

	public static function getAppDirectory() {
		return self::$appDirectory;
	}

	public static function getAppAssetsDirectory() {
		return self::$appAssetsDirectory;
	}

	public static function getStorageDirectory() {
		return self::$storageDirectory;
	}

	public static function getWwwDirectory() {
		return self::$wwwDirectory;
	}

	public static function getCacheDirectory() {
		return self::getStorageDirectory() . self::$cacheDirectory;
	}

	public static function getUserCacheDirectory() {
		return self::getStorageDirectory() . self::$cacheDirectory . session_id();
	}

	public static function getDiskDirectory() {
		return self::getStorageDirectory() . self::$diskDirectory;
	}

	public static function getStaticDirectory() {
		return self::getStorageDirectory() . self::$staticDirectory;
	}

	/**
	 * For compiling static assets
	 * that are raw JS and CSS files
	 */
	public static function compileAssets() {
		echo '🐘 Elephaime - Assets script' . PHP_EOL;

		echo "🧹 Cleaning cache and static files directory..." . PHP_EOL;
		self::rmdirRecursive(self::getCacheDirectory(), true);
		self::rmdirRecursive(self::getStaticDirectory(), true);

		echo "📂 Creating static directory..." . PHP_EOL;
		$storageDir = 'storage/';
		$excludedFiles = Initializer::$excludedFiles;
		Initializer::copyDefaultFilesRecursive($storageDir, $excludedFiles);

		// Static images
		echo "📂 Creating and filling static images directory..." . PHP_EOL;
		mkdir(self::getStaticDirectory() . 'images/');
		self::copyDirRecursive(self::getAppAssetsDirectory() . 'images/', self::getStaticDirectory() . 'images/', true);

		// Static fonts
		echo "📂 Creating and filling static fonts directory..." . PHP_EOL;
		mkdir(self::getStaticDirectory() . 'fonts/');
		self::copyDirRecursive(self::getAppAssetsDirectory() . 'fonts/', self::getStaticDirectory() . 'fonts/', true);

		// CSS
		echo "📂 Creating and filling static CSS directory..." . PHP_EOL;
		mkdir(self::getStaticDirectory() . 'css/');
		self::copyDirRecursive(self::getAppAssetsDirectory() . 'scss/', self::getStaticDirectory() . 'css/', true);

		// JavaScript
		echo "📂 Creating and filling static JavaScript directory..." . PHP_EOL;
		mkdir(self::getStaticDirectory() . 'js/');
		self::copyDirRecursive(self::getAppAssetsDirectory() . 'js/', self::getStaticDirectory() . 'js/', true);
	}

	/**
	 * Create the required system links to storage directories in www
	 */
	public static function makeWwwLinks() {
		$storageDir = 'storage' . DIRECTORY_SEPARATOR;
		$wwwDir = 'www' . DIRECTORY_SEPARATOR;

		// Create a symlink for static assets
		echo "Creating a link to the static resources folder..." . PHP_EOL;
		@unlink(realpath(PROJECT_PATH . $wwwDir) . DIRECTORY_SEPARATOR . 'static');
		self::link(
			realpath(PROJECT_PATH . $storageDir . 'static'),
			realpath(PROJECT_PATH . $wwwDir) . DIRECTORY_SEPARATOR . 'static'
		);

		// Create a symlink for dynamic files
		echo "Creating a link to the storage resources folder..." . PHP_EOL;
		@unlink(realpath(PROJECT_PATH . $wwwDir) . DIRECTORY_SEPARATOR . 'storage');
		self::link(
			realpath(PROJECT_PATH . $storageDir . 'disk'),
			realpath(PROJECT_PATH . $wwwDir) . DIRECTORY_SEPARATOR . 'storage'
		);
	}

	/**
	 * Create a symlink (for Linux and Windows based OS)
	 * @param string $target
	 * @param string $link
	 */
	public static function link(string $target, string $link) {
		if (PHP_OS_FAMILY !== 'Windows') {
			return symlink($target, $link);
		}
		exec("mklink /J ".escapeshellarg($link).' '.escapeshellarg($target));
	}

	/**
	 * Retrieve an asset relative URL
	 * @param string $asset		The asset name
	 */
	public static function asset(string $asset) {
		$asset = self::sanitizeFilePath($asset);
		$appRoot = env('APP_ROOT');
		$returnURL = $appRoot . '/' . $asset;
		if (file_exists(self::getDiskDirectory() . $asset)) {
			$returnURL = $appRoot . '/storage/' . $asset;
		} else if (file_exists(self::getStaticDirectory() . $asset)) {
			$returnURL = $appRoot . '/static/' . $asset;
		}
		return str_replace('//', '/', $returnURL);
	}

	/**
	 * Read the content a specified file
	 * @param string $path
	 */
	public static function read(string $path) {
		$path = self::sanitizeFilePath($path);
		if (file_exists(self::getDiskDirectory() . $path)) {
			return file_get_contents(self::getDiskDirectory() . $path);
		}
		return null;
	}

	/**
	 * Save a file with the specified path/name and content
	 * @param string $path
	 * @param mixed $content
	 */
	public static function write(string $path, $content) {
		$path = self::sanitizeFilePath($path);
		$pathDirs = explode('/', $path);
		$newPath = self::getDiskDirectory();
		foreach ($pathDirs as $pathPart) {
			if (empty($pathPart))
				continue;
			$newPath .= '/' . $pathPart;
			if (array_search($pathPart, $pathDirs, true) < (count($pathDirs) - 1) && !is_dir($newPath)) {
				mkdir($newPath);
			}
		}
		return file_put_contents($newPath, $content);
	}

	/**
	 * Store an uploaded file in a specified place
	 * @param string $newPath
	 * @param array $uploadedFile
	 */
	public static function upload(string $newPath, array $uploadedFile) {
		$fileContent = file_get_contents($uploadedFile['tmp_name']);
		$newPath = self::sanitizeFilePath($newPath);
		return self::write($newPath, $fileContent);
	}

	/**
	 * Delete a specified file
	 * @param string $path
	 */
	public static function delete(string $path) {
		$path = self::sanitizeFilePath($path);
		if (file_exists(self::getDiskDirectory() . $path)) {
			return unlink(self::getDiskDirectory() . $path);
		}
		return false;
	}
}
