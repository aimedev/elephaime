<?php

	if (is_dir(__DIR__ . '/../vendor')) { // As Composer package
		require __DIR__ . '/../vendor/aimedev/elephaime/src/run.php';

	} else { // As Git submodule
		require __DIR__ . '/../elephaime/src/run.php';
	}
