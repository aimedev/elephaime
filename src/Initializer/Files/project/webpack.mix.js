// webpack.mix.js

let mix = require('laravel-mix');

// OS notifications
mix.disableNotifications();

// Static images
mix.copy('app/Assets/images/*', 'storage/static/images');

// Static fonts
mix.copy('app/Assets/fonts/*', 'storage/static/fonts');

// CSS
mix.sass('app/Assets/scss/_index.scss', 'storage/static/css');

// JavaScript
mix.js('app/Assets/js/_index.js', 'storage/static/js');
