<?php

/**
 * Register HTTP routes
 *
 * This is the place for your HTTP routes !
 * Please open the README file for more information.
 */

routes()->get('/', 'HomeController>greeting', [])
	->after('HomeController>elephantSound');
