<?php


/**
 * Register console actions
 *
 * This is the place for your cron jobs !
 * Please open the README file for more information.
 */

console()->task('at 00:00', function() {
	print "New day, new problems!";
});

console()->task('every 1 minute', function() {
	$currentMinute = date('i');
	print "🐛 One bug found in this minute! ($currentMinute)";
});
