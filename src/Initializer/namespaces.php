<?php

/**
 * Loader for Elephaime engine's namespaces
 */

use Aimedev\Elephaime\Core\Storage;

define('ELEPHAIME_PATH', realpath(__DIR__ . '/..') . '/');
define('ELEPHAIME_WITH_COMPOSER', file_exists(ELEPHAIME_PATH . '../../../autoload.php'));
define('ELEPHAIME_INIT_PATH', ELEPHAIME_PATH . 'Initializer/');

if (ELEPHAIME_WITH_COMPOSER === true) {
	require ELEPHAIME_PATH . '../../../autoload.php';
	define('PROJECT_PATH', realpath(ELEPHAIME_PATH . '../../../..') . '/');
} else {
	define('PROJECT_PATH', realpath(ELEPHAIME_PATH . '../..') . '/');
}

define('PROJECT_APP_PATH', PROJECT_PATH . 'app/');


global $systemPaths; // Can be useful
$systemPaths = (object) [
	"elephaime" => ELEPHAIME_PATH,
	"app" => Storage::getAppDirectory(),
	"controllers" => Storage::getAppDirectory() . 'Controllers/',
	"models" => Storage::getAppDirectory() . 'Models/',
	'helpers' => Storage::getAppDirectory() . 'Helpers/',
	'views' => Storage::getAppAssetsDirectory() . 'views/'
];

$rootNamespace = "App\\";
$appNamespaces = [
	$rootNamespace . "Controllers" => $systemPaths->controllers,
	$rootNamespace . "Models" => $systemPaths->models,
	$rootNamespace . "Helpers" => $systemPaths->helpers,
	$rootNamespace . "Views" => $systemPaths->views
];

/**
 * We manually load app resources as namespaces (to gain access to their classes)
 * @Wilkins (https://github.com/Wilkins) (https://stackoverflow.com/a/39774973)
 */
function __loadCustomNamespaces(array $namespaces) {
	foreach ($namespaces as $namespace => $classpaths) {
		if (!is_array($classpaths))
			$classpaths = array($classpaths);
		spl_autoload_register(function ($classname) use ($namespace, $classpaths) {
			if (preg_match("#^".preg_quote($namespace)."#", $classname)) {
				$classname = str_replace($namespace, "", $classname);
				$filename = preg_replace("#\\\\#", "/", $classname) . ".php";
				foreach ($classpaths as $classpath) {
					$fullpath = $classpath . "/$filename";
					if (file_exists($fullpath))
						include_once $fullpath;
				}
			}
		});
	}
}

if (ELEPHAIME_WITH_COMPOSER !== true) {
	__loadCustomNamespaces($appNamespaces);
	__loadCustomNamespaces([
		'Aimedev\\Elephaime\\' => $systemPaths->elephaime
	]);
}
