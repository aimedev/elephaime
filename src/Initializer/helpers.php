<?php

use Aimedev\Elephaime\Core\Console\ConsoleRegistrar;
use Aimedev\Elephaime\Core\Database\DatabaseConnection;
use Aimedev\Elephaime\Core\Database\DatabaseManager;
use Aimedev\Elephaime\Core\Eleph;
use Aimedev\Elephaime\Core\Env;
use Aimedev\Elephaime\Core\ErrorHandler;
use Aimedev\Elephaime\Core\Language;
use Aimedev\Elephaime\Core\Render\Page;
use Aimedev\Elephaime\Core\Routing\RouteRegistrar;
use Aimedev\Elephaime\Core\Storage;

function eleph(): Eleph {
	$eleph = Aimedev\Elephaime\Core\Eleph::self();
	return $eleph;
}

function asset(string $asset): string {
	return \Aimedev\Elephaime\Core\Storage::asset($asset);
}

function console(): ConsoleRegistrar {
	$eleph = Eleph::self();
	if (empty($eleph->console))
		$eleph->console = new ConsoleRegistrar();
	return $eleph->console;
}

function database(string $dbname = ''): DatabaseConnection {
	$eleph = Eleph::self();
	if (empty($eleph->database))
		$eleph->database = new DatabaseManager();
	return $eleph->database->instance($dbname);
}

function env(string $key) {
	$eleph = Eleph::self();
	if (empty($eleph->env))
		$eleph->env = new Env(PROJECT_PATH . '/.env');
	return $eleph->env->val($key);
}

function errors(): ErrorHandler {
	$eleph = Eleph::self();
	if (empty($eleph->errors))
		$eleph->errors = new ErrorHandler();
	return $eleph->errors;
}

function fatal(int $errorCode, string $message = 'Server error', array $contextVariables = []): never {
	errors()->throw($errorCode, $message, $contextVariables);
}

function lang(): Language {
	$eleph = Eleph::self();
	if (empty($eleph->lang)) {
		$eleph->lang = new Language();
		if ($eleph->lang->isAvailable()) {
			$eleph->lang->setAcceptedLanguages(['en', 'fr']);
			$eleph->lang->setLocale('en');
		}
	}
	return $eleph->lang;
}

function page(): Page {
	$eleph = Eleph::self();
	if (empty($eleph->page))
		$eleph->page = new Page();
	return $eleph->page;
}

function routes(): RouteRegistrar {
	$eleph = Eleph::self();
	if (empty($eleph->routing))
		$eleph->routing = new RouteRegistrar();
	return $eleph->routing;
}

function storage(): Storage {
	$eleph = Eleph::self();
	if (empty($eleph->storage))
		$eleph->storage = new Storage();
	return $eleph->storage;
}
