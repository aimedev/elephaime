<title><?= $message ?></title>
<div class="error-page">
	<h1><?= $code ?> elephants</h1>
	<p>
		<?= $message ?>
	</p>
	<?php if (!empty($context)) { ?>
		<h3>Context</h3>
		<div class="simple-table">
			<?php foreach ($context as $key => $value) { ?>
				<div class="row">
					<div class="col"><?= $key ?></div>
					<div class="col"><?= $value ?></div>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<?php if (!empty($stacktrace)) { ?>
		<h3>Stacktrace</h3>
		<div class="simple-table">
			<?php foreach ($stacktrace as $entry) { ?>
				<div class="row">
					<div class="col"><?= $entry ?></div>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<div class="links">
		<a href="https://gitlab.com/aimedev/elephaime/-/blob/main/USAGE.md" target="_blank">How to..?</a>
		<a target="_blank" href="https://gitlab.com/aimedev/elephaime/-/issues">Report a core problem</a>
	</div>
</div>

<style>

	@import url('https://fonts.googleapis.com/css2?family=Macondo&display=swap');

	:root {
		--body-background: #9aacb6;
	}

	* {
		box-sizing: border-box;
		scroll-behavior: smooth;
	}

	body {
		margin: 0;
		background-color: var(--body-background);
		font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
		font-size: 16px;
	}

	.error-page {
		width: 100vw;
		height: 100vh;
		display: flex;
		flex-direction: column;
		justify-content: start;
		align-items: center;
		text-align: center;
		padding: 150px 20px;
		overflow: auto;
	}

	.error-page > * {
		margin-left: 25px;
		margin-right: 25px;
		max-width: 800px;
		width: 100%;
	}

	.error-page > h1 {
		font-family: 'Macondo', sans-serif;
	}

	.error-page > .simple-table {
		margin-bottom: 30px;
		text-align: left;
		border-bottom: 1px solid #444;
	}
	.error-page > .simple-table > .row {
		background-color: rgba(0, 0, 0, .05);
		border: 1px solid #444;
		border-bottom-width: 0;
		border-right-width: 0;
		display: flex;
	}
	.error-page > .simple-table > .row:nth-child(even) {
		background-color: transparent;
	}
	.error-page > .simple-table > .row > .col {
		padding: 10px;
		flex: 1;
		border-right: 1px solid #444;
	}

	.error-page > .links > * {
		font-weight: bold;
		margin: 20px 20px;
		display: inline-block;
		color: #2e2e7e;
	}
</style>
